package Test;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import calculadora.CalculosBasicos;

public class sumaTest {
	CalculosBasicos calculadora;

	@BeforeClass
	
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void inicializar(){
		calculadora=new CalculosBasicos();
		

	}

	@Test
	public void sumaSimple() {
		
		calculadora.ingreso('1');
		calculadora.operador("+");
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(3, resultado,0);
	}
	
	@Test
	public void sumaSimple1() {
		
		calculadora.ingreso('1');
		calculadora.ingreso('0');
		calculadora.operador("+");
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(12, resultado,0);
	}
	
	@Test
	public void sumaSimple2(){
		calculadora.ingreso('2');
		calculadora.ingreso('.');
		calculadora.ingreso('5');
		calculadora.operador("+");
		calculadora.ingreso('0');
		calculadora.ingreso('.');
		calculadora.ingreso('5');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(3, resultado,0);
	}
	
	@Test
	public void sumaConDosSignos() {
		
		calculadora.operador("-");
		calculadora.ingreso('1');
		calculadora.operador("+");
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(3, resultado,0);
	}
	
	@Test
	public void sumaConDosSignos2() {
		
		calculadora.ingreso('1');
		calculadora.operador("+");
		calculadora.operador("-");
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(3, resultado,0);
	}
	
	@Test
	public void sumaConTresSignos() {
		
		calculadora.operador("-");
		calculadora.ingreso('1');
		calculadora.operador("-");
		calculadora.operador("+");
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-1, resultado,0);
	}
	
	@Test
	public void sumaConTresSignos1() {
		
		calculadora.operador("-");
		calculadora.operador("-");
		calculadora.ingreso('1');
		calculadora.operador("+");
		calculadora.ingreso('2');
		calculadora.operador("=");;
		float resultado=Float.valueOf(calculadora.mostrar());
		
		assertEquals(3, resultado,0);
	}
	
	@Test
	public void sumaConTresSignos2() {
	
		calculadora.ingreso('2');
		calculadora.operador("+");
		calculadora.operador("-");
		calculadora.operador("-");
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(4, resultado,0);
	}
	
	@Test
	public void sumaConMasSignos() {
		
		calculadora.operador("-");
		calculadora.operador("-");
		calculadora.ingreso('1');
		calculadora.operador("+");
		calculadora.operador("-");
		calculadora.operador("-");
		calculadora.ingreso('5');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(6, resultado,0);
	}
	
	@Test
	public void sumaConMasSignos2() {
		
		calculadora.operador("-");
		calculadora.operador("-");
		calculadora.operador("-");
		calculadora.ingreso('1');
		calculadora.operador("+");
		calculadora.operador("-");
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(3, resultado,0);
	}
	
	@Test
	public void sumaConMasSignos3() {
		
		calculadora.operador("-");
		calculadora.ingreso('1');
		calculadora.operador("+");
		calculadora.operador("-");
		calculadora.operador("-");
		calculadora.operador("-");
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(3, resultado,0);
	}
	
	@Test
	public void sumaConCero() {
		
		calculadora.ingreso('4');
		calculadora.operador("+");
		calculadora.ingreso('0');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(4, resultado,0);
	}
	
	@Test
	public void sumaConDecimales() {
		
		calculadora.ingreso('1');
		calculadora.ingreso('.');
		calculadora.ingreso('9');
		
		calculadora.operador("+");
		calculadora.ingreso('2');
		calculadora.ingreso('.');
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(4.1, resultado,0);
	}
	
	@Test
	public void sumaConDecimales1() {
		
		calculadora.ingreso('-');	
		calculadora.ingreso('4');
		calculadora.ingreso('.');
		calculadora.ingreso('1');
		
		calculadora.operador("+");
		calculadora.ingreso('1');
		calculadora.ingreso('4');
		calculadora.ingreso('.');
		calculadora.ingreso('7');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(10.6, resultado,0);
	}
		
	

}



