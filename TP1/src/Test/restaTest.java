package Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import calculadora.CalculosBasicos;

public class restaTest {
	CalculosBasicos calculadora;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	
	
	@Before
	public void inicializar(){
		calculadora=new CalculosBasicos();

	}

	@Test
	public void restaSimple() {
				
		calculadora.ingreso('1');
		calculadora.operador("-");
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-1, resultado,0);
}
	
	@Test
	public void restaSimple1() {
			
		calculadora.ingreso('2');
		calculadora.operador("-");
		calculadora.ingreso('1');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(1, resultado,0);
}
	@Test
	public void restaSimple2() {
			
		calculadora.ingreso('2');
		calculadora.ingreso('.');
		calculadora.ingreso('5');
		calculadora.operador("-");
		calculadora.ingreso('0');
		calculadora.ingreso('.');
		calculadora.ingreso('5');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(2, resultado,0);
}

	@Test
	public void restaConDosSignos() {	
	
		calculadora.ingreso('1');
		calculadora.operador("-");
		calculadora.operador("-");
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-1, resultado,0);
}
	
	@Test
	public void restaConDosSignos1() {
		
		calculadora.ingreso('-');
		calculadora.ingreso('1');
		calculadora.operador("-");
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-3, resultado,0);
}
	
	@Test
	public void restaConTresSignos() {
		
		calculadora.ingreso('-');
		calculadora.ingreso('1');
		calculadora.operador("-");
		calculadora.operador("-");
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-3, resultado,0);
}
	
	@Test
	public void restaConTresSignos1() {
		
		calculadora.ingreso('-');
		calculadora.ingreso('-');
		calculadora.ingreso('1');
		calculadora.operador("-");
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
	
		assertEquals(-3, resultado,0);
}
	
	@Test
	public void restaConTresSignos2() {
		
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("-");
		calculadora.operador("-");
		calculadora.ingreso('1');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-3, resultado,0);
}
	
	@Test
	public void restaConTresSignos3() {
		
		calculadora.ingreso('-');
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("-");
		calculadora.ingreso('1');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
	
		assertEquals(-3, resultado,0);
}

	@Test
	public void restaConCeros() {
		
		calculadora.ingreso('0');
		calculadora.operador("-");
		calculadora.ingreso('0');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(0, resultado,0);
}
	
	@Test
	public void restaConCeros1() {
		
		
		calculadora.ingreso('0');
		calculadora.operador("-");
		calculadora.operador("-");
		calculadora.ingreso('0');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(0, resultado,0);
}
	
	@Test
	public void restaConCeros2() {
		
		calculadora.ingreso('-');
		calculadora.ingreso('0');
		calculadora.operador("-");
		calculadora.operador("-");
		calculadora.ingreso('0');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(0, resultado,0);
}
	@Test
	public void restaConDecimales() {
		
		calculadora.ingreso('0');
		calculadora.ingreso('.');
		calculadora.ingreso('9');
		
		calculadora.operador("-");
		calculadora.ingreso('3');
		calculadora.ingreso('.');
		calculadora.ingreso('2');
		calculadora.ingreso('6');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-2.36, resultado,0);
	}
	
	@Test
	public void restaConDecimales1() {
		
		calculadora.ingreso('-');
		calculadora.ingreso('9');
		calculadora.ingreso('.');
		calculadora.ingreso('3');
		calculadora.operador("-");
		calculadora.ingreso('-');
		calculadora.ingreso('3');
		calculadora.ingreso('.');
		calculadora.ingreso('2');
		calculadora.ingreso('6');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-6.04, resultado,0);
	}
	


}
