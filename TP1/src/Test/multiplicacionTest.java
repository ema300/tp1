package Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import calculadora.CalculosBasicos;

public class multiplicacionTest {

	CalculosBasicos calculadora;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void inicializar(){
		calculadora=new CalculosBasicos();
		

	}
	
	@Test
	public void multiplicacionSimple() {
	
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('5');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(10, resultado,0);
	}
	
	
	
	
	@Test
	public void multiplicacionSimple2() {
	
		calculadora.ingreso('5');
		calculadora.operador("*");
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(10, resultado,0);

	}
	
	@Test
	public void multiplicacionConDosSignos() {
		
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('5');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-10, resultado,0);
	}
	
	@Test
	public void multiplicacionConDosSignos1() {
	
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('-');
		calculadora.ingreso('5');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-10, resultado,0);
	}
	
	@Test
	public void multiplicacionConTresSignos() {
	
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('-');
		calculadora.ingreso('5');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(10, resultado,0);
	}
	@Test
	public void multiplicacionConTresSignos1() {
	
		calculadora.ingreso('-');
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('5');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-10, resultado,0);
	}

	@Test
	public void multiplicacionConTresSignos2() {
		
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('-');
		calculadora.ingreso('-');
		calculadora.ingreso('5');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-10, resultado,0);
	}
	
	@Test
	public void multiplicacionConMasSignos() {
	
		calculadora.ingreso('-');
		calculadora.ingreso('-');
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('5');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-10, resultado,0);
	}
	
	@Test
	public void multiplicacionConMasSignos1() {
		
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('-');
		calculadora.ingreso('-');
		calculadora.ingreso('-');
		calculadora.ingreso('5');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-10, resultado,0);
	}
	
	@Test
	public void multiplicacionConMasSignos2() {
	
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('-');
		calculadora.ingreso('-');
		calculadora.ingreso('5');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
	
		assertEquals(10, resultado,0);
	}
	
	@Test
	public void multiplicacionConMasSignos3() {
	
		calculadora.ingreso('-');
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('-');
		calculadora.ingreso('5');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
	
		assertEquals(10, resultado,0);
	}

	@Test
	public void multiplicacionConCeros() {
		
		calculadora.ingreso('0');
		calculadora.operador("*");
		calculadora.ingreso('5');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(0, resultado,0);
	}
	
	@Test
	public void multiplicacionConCeros1() {
		
		calculadora.ingreso('5');
		calculadora.operador("*");
		calculadora.ingreso('0');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
	
		assertEquals(0, resultado,0);
	}
	
	@Test
	public void multiplicacionConCeros2() {
	
		calculadora.ingreso('-');
		calculadora.ingreso('5');
		calculadora.operador("*");
		calculadora.ingreso('0');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(0, resultado,0);
	}
	
	@Test
	public void multiplicacionConCeros3() {
	
		calculadora.ingreso('0');
		calculadora.operador("*");
		calculadora.ingreso('-');
		calculadora.ingreso('5');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(0, resultado,0);

	}
	
	@Test
	public void multiplicacionConDecimales() {
		
		calculadora.ingreso('0');
		calculadora.ingreso('.');
		calculadora.ingreso('1');
		calculadora.ingreso('8');
		calculadora.ingreso('0');
		calculadora.operador("*");
		calculadora.ingreso('-');
		calculadora.ingreso('1');
		calculadora.ingreso('2');
		calculadora.ingreso('.');
		calculadora.ingreso('7');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-2.28, resultado,0.1);
	}
	
	@Test
	public void multiplicacionConDecimales1() {
		
		calculadora.ingreso('-');
		calculadora.ingreso('0');
		calculadora.ingreso('.');
		calculadora.ingreso('2');
		calculadora.ingreso('1');
		calculadora.operador("*");
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.ingreso('.');
		calculadora.ingreso('8');
		calculadora.ingreso('3');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(0.59, resultado,0.1);
	}

	
}
