package Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import calculadora.CalculosBasicos;

public class divisionTest {

	CalculosBasicos calculadora;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void inicializar(){
		calculadora=new CalculosBasicos();
		

	}
	
	@Test
	public void divisionSimple() {
		
		calculadora.ingreso('1');
		calculadora.operador("/");
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(0.5, resultado,0);
	}
	
	@Test
	public void divisionSimple1() {
		
		calculadora.ingreso('2');
		calculadora.operador("/");
		calculadora.ingreso('1');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(2, resultado,0);
	}
	
	@Test
	public void divisionConDosSignos() {
		
		calculadora.ingreso('-');
		calculadora.ingreso('1');
		calculadora.operador("/");
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-0.5, resultado,0);
	}
	
	@Test
	public void divisionConDosSignos1() {
		
		calculadora.ingreso('1');
		calculadora.operador("/");
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-0.5, resultado,0);
	}
	
	@Test
	public void divisionConTresSignos() {
		
		calculadora.ingreso('-');
		calculadora.ingreso('1');
		calculadora.operador("/");
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(0.5, resultado,0);
	}
	
	@Test
	public void divisionConTresSignos1() {
		
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("/");
		calculadora.ingreso('-');
		calculadora.ingreso('1');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(2, resultado,0);
	}
	
	@Test
	public void divisionConTresSignos2() {
		
		calculadora.ingreso('-');
		calculadora.ingreso('-');
		calculadora.ingreso('1');
		calculadora.operador("/");
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-0.5, resultado,0);
	}
	
	@Test
	public void divisionConTresSignos3() {
		
		calculadora.ingreso('1');
		calculadora.operador("/");
		calculadora.ingreso('-');
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-0.5, resultado,0);
	}
	
	@Test
	public void divisionConMasSignos() {
		
		calculadora.ingreso('-');
		calculadora.ingreso('1');
		calculadora.operador("/");
		calculadora.ingreso('-');
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(0.5, resultado,0);
	}
	
	@Test
	public void divisionConMasSignos1() {
			
		calculadora.ingreso('1');
		calculadora.operador("/");
		calculadora.ingreso('-');
		calculadora.ingreso('-');
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-0.5, resultado,0);
	}
	
	@Test
	public void divisionConMasSignos2() {
		
		calculadora.ingreso('-');
		calculadora.ingreso('-');
		calculadora.ingreso('1');
		calculadora.operador("/");
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(0.5, resultado,0);
	}
	
	@Test
	public void divisionConCero() {
		
		calculadora.ingreso('0');
		calculadora.operador("/");
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(0, resultado,0);
	}
	
	@Test
	public void divisionConCero1() {
		
		calculadora.ingreso('0');
		calculadora.operador("/");
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(0, resultado,0);
	}
	
	@Test
	public void divisionConCero2() {
		
		calculadora.ingreso('0');
		calculadora.operador("/");
		calculadora.ingreso('-');
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(0, resultado,0);
	}
	
	@Test
	public void divisionConCero3() {
		
		calculadora.ingreso('-');
		calculadora.ingreso('0');
		calculadora.operador("/");
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(0, resultado,0);
	}
	
	@Test
	public void divisionConDecimales() {
		
		calculadora.ingreso('1');
		calculadora.ingreso('2');
		
		
		calculadora.operador("/");
		calculadora.ingreso('1');
		calculadora.ingreso('3');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(0.9, resultado,0.1);
	}
	
	@Test
	public void divisionConDecimales1() {
		
		calculadora.ingreso('1');
		calculadora.ingreso('2');
		calculadora.ingreso('.');
		calculadora.ingreso('5');
		calculadora.operador("/");
		calculadora.ingreso('4');
		calculadora.ingreso('7');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(0.26, resultado,0.1);
	
	}
	
	@Test
	public void divisionConDecimales2() {
		
		calculadora.ingreso('4');
		calculadora.ingreso('7');
		calculadora.operador("/");
		calculadora.ingreso('2');
		calculadora.ingreso('.');
		calculadora.ingreso('4');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(19.58, resultado,0.1);
	}
	
	@Test
	public void divisionConDecimales3() {
		
		calculadora.ingreso('1');
		calculadora.ingreso('8');
		calculadora.ingreso('0');
		calculadora.ingreso('.');
		calculadora.ingreso('1');
		calculadora.ingreso('9');
		calculadora.operador("/");
		calculadora.ingreso('2');
		calculadora.ingreso('1');
		calculadora.ingreso('5');
		calculadora.ingreso('.');
		calculadora.ingreso('2');
		calculadora.ingreso('1');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(0.83, resultado,0.1);
	}
	
	@Test
	public void divisionConDecimales4() {
		
		calculadora.ingreso('-');
		calculadora.ingreso('1');
		calculadora.ingreso('1');
		calculadora.ingreso('0');
		calculadora.ingreso('.');
		calculadora.ingreso('3');
		calculadora.operador("/");
		calculadora.ingreso('1');
		calculadora.ingreso('1');
		calculadora.ingreso('5');
		calculadora.ingreso('.');
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());
		
		assertEquals(-0.95, resultado,0.1);
	}
	
	
}
