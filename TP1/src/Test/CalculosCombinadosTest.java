package Test;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import calculadora.CalculosBasicos;



public class CalculosCombinadosTest {

	CalculosBasicos calculadora;

	@BeforeClass

	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void inicializar(){
		calculadora=new CalculosBasicos();
		//calculadora.operador("MC");

	}

	@Test
	public void calculo1() {

		calculadora.ingreso('2');
		calculadora.operador("+");
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('3');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(8, resultado,0);
	}
	@Test
	public void calculo2() {

		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('3');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(12, resultado,0);
	}

	@Test
	public void calculo3() {

		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('2');
		calculadora.operador("+");
		calculadora.ingreso('3');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(7, resultado,0);
	}

	@Test
	public void calculo4() {

		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('2');
		calculadora.operador("+");
		calculadora.ingreso('3');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(-1, resultado,0);
	}

	@Test
	public void calculo5() {

		calculadora.ingreso('-');
		calculadora.ingreso('3');
		calculadora.operador("+");
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('3');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(3, resultado,0);
	}

	@Test
	public void calculo6() {

		calculadora.ingreso('-');
		calculadora.ingreso('3');
		calculadora.operador("+");
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('-');
		calculadora.ingreso('3');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(-9, resultado,0);
	}

	@Test
	public void calculo7() {

		calculadora.ingreso('3');
		calculadora.operador("+");
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('-');
		calculadora.ingreso('3');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(-3, resultado,0);
	}

	@Test
	public void calculo8() {

		calculadora.ingreso('3');
		calculadora.operador("+");
		calculadora.ingreso('2');
		calculadora.operador("+");
		calculadora.ingreso('3');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(8, resultado,0);
	}

	@Test
	public void calculo9() {

		calculadora.ingreso('3');
		calculadora.operador("-");
		calculadora.ingreso('2');
		calculadora.operador("-");
		calculadora.ingreso('3');
		calculadora.operador("-");
		calculadora.ingreso('1');
		calculadora.ingreso('0');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(-12, resultado,0);
	}

	@Test
	public void calculo10() {

		calculadora.ingreso('3');
		calculadora.operador("+");
		calculadora.ingreso('2');
		calculadora.operador("+");
		calculadora.ingreso('3');
		calculadora.operador("+");
		calculadora.ingreso('1');
		calculadora.ingreso('0');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(18, resultado,0);
	}

	@Test
	public void calculo11() {

		calculadora.ingreso('5');
		calculadora.operador("+");
		calculadora.ingreso('2');
		calculadora.operador("-");
		calculadora.ingreso('6');
		calculadora.operador("+");
		calculadora.ingreso('1');
		calculadora.ingreso('0');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(11, resultado,0);
	}

	@Test
	public void calculo12() {

		calculadora.ingreso('5');
		calculadora.operador("-");
		calculadora.ingreso('2');
		calculadora.operador("+");
		calculadora.ingreso('6');
		calculadora.operador("-");
		calculadora.ingreso('1');
		calculadora.ingreso('0');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(-1, resultado,0);
	}

	@Test
	public void calculo13() {

		calculadora.ingreso('5');
		calculadora.operador("-");
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('6');
		calculadora.operador("-");
		calculadora.ingreso('1');
		calculadora.ingreso('0');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(-17, resultado,0);
	}

	@Test
	public void calculo14() {

		calculadora.ingreso('2');
		calculadora.operador("+");
		calculadora.ingreso('6');
		calculadora.operador("/");
		calculadora.ingreso('3');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(4, resultado,0);
	}
	@Test
	public void calculo15() {

		calculadora.ingreso('8');
		calculadora.operador("/");
		calculadora.ingreso('2');
		calculadora.operador("/");
		calculadora.ingreso('2');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(2, resultado,0);
	}

	@Test
	public void calculo16() {

		calculadora.ingreso('2');
		calculadora.operador("/");
		calculadora.ingreso('2');
		calculadora.operador("+");
		calculadora.ingreso('3');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(4, resultado,0);
	}

	@Test
	public void calculo17() {

		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("/");
		calculadora.ingreso('2');
		calculadora.operador("+");
		calculadora.ingreso('3');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(2, resultado,0);
	}

	@Test
	public void calculo18() {

		calculadora.ingreso('-');
		calculadora.ingreso('4');
		calculadora.operador("+");
		calculadora.ingreso('4');
		calculadora.operador("/");
		calculadora.ingreso('2');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(-2, resultado,0);
	}

	@Test
	public void calculo19() {

		calculadora.ingreso('-');
		calculadora.ingreso('3');
		calculadora.operador("+");
		calculadora.ingreso('6');
		calculadora.operador("/");
		calculadora.ingreso('-');
		calculadora.ingreso('3');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(-5, resultado,0);
	}

	@Test
	public void calculo20() {

		calculadora.ingreso('3');
		calculadora.operador("+");
		calculadora.ingreso('6');
		calculadora.operador("/");
		calculadora.ingreso('-');
		calculadora.ingreso('3');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(1, resultado,0);
	}

	@Test
	public void calculo21() {

		calculadora.ingreso('1');
		calculadora.operador("+");
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('3');
		calculadora.operador("*");
		calculadora.ingreso('4');
		calculadora.operador("/");
		calculadora.ingreso('2');
		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(13, resultado,1);
	}

	@Test
	public void calculo22() {

		calculadora.ingreso('1');
		calculadora.operador("+");
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('3');
		calculadora.operador("*");
		calculadora.ingreso('4');
		calculadora.operador("*");
		calculadora.ingreso('2');
		calculadora.operador("-");
		calculadora.ingreso('1');
		calculadora.ingreso('0');

		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(39, resultado,1);
	}
	@Test
	public void calculo23() {

		calculadora.ingreso('1');
		calculadora.operador("+");
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('3');
		calculadora.operador("*");
		calculadora.ingreso('4');
		calculadora.operador("+");
		calculadora.ingreso('2');
		calculadora.operador("*");
		calculadora.ingreso('3');

		calculadora.operador("=");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(31, resultado,1);
	}

	@Test
	public void calculo24() {

		calculadora.ingreso('2');
		calculadora.ingreso('8');
		calculadora.ingreso('0');
		calculadora.operador("/");
		calculadora.ingreso('1');
		calculadora.ingreso('0');
		calculadora.ingreso('0');
		calculadora.ingreso('0');
		calculadora.operador("*");
		calculadora.ingreso('4');
		calculadora.ingreso('5');
		calculadora.ingreso('.');
		calculadora.ingreso('9');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(12.8, resultado,0.1);
	}

	@Test
	public void calculo25() {

		calculadora.ingreso('-');
		calculadora.ingreso('8');
		calculadora.ingreso('0');
		calculadora.operador("+");
		calculadora.ingreso('1');
		calculadora.ingreso('0');
		calculadora.ingreso('0');
		calculadora.ingreso('0');
		calculadora.operador("/");
		calculadora.ingreso('4');
		calculadora.ingreso('5');
		calculadora.ingreso('9');
		calculadora.ingreso('0');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(-79.7, resultado,0.1);
	}

	@Test
	public void calculo26() {

		calculadora.ingreso('1');
		calculadora.ingreso('2');
		calculadora.ingreso('.');
		calculadora.ingreso('5');
		calculadora.operador("/");
		calculadora.ingreso('1');
		calculadora.ingreso('3');
		calculadora.operador("+");
		calculadora.ingreso('0');
		calculadora.ingreso('.');
		calculadora.ingreso('7');
		calculadora.ingreso('7');
		calculadora.operador("*");
		calculadora.ingreso('-');
		calculadora.ingreso('4');
		calculadora.ingreso('0');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(-29.8, resultado,0.1);
	}


	@Test
	public void calculo27() {

		calculadora.ingreso('-');
		calculadora.ingreso('9');
		calculadora.operador("*");
		calculadora.ingreso('-');
		calculadora.ingreso('0');
		calculadora.ingreso('.');
		calculadora.ingreso('1');
		calculadora.ingreso('2');
		calculadora.operador("/");
		calculadora.ingreso('-');
		calculadora.ingreso('5');
		calculadora.ingreso('.');
		calculadora.ingreso('5');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(-0.19, resultado,0.01);
	}

	@Test
	public void calculosConLaMemoria() {

		calculadora.ingreso('2');
		calculadora.operador("M+");
		calculadora.borrarTodo();
		calculadora.ingreso('5');
		calculadora.operador("M+");
		calculadora.borrarTodo();
		calculadora.ingreso('3');
		calculadora.operador("M-");
		calculadora.borrarTodo();
		calculadora.ingreso('3');
		calculadora.operador("*");
		calculadora.operador("MR");
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(12, resultado,0);
	}

	@Test
	public void calculosConLaMemoria1() {

		calculadora.ingreso('2');
		calculadora.operador("M-");
		calculadora.borrarTodo();
		calculadora.ingreso('5');
		calculadora.operador("M+");
		calculadora.borrarTodo();
		calculadora.ingreso('1');
		calculadora.operador("M-");
		calculadora.borrarTodo();
		calculadora.ingreso('3');
		calculadora.operador("*");
		calculadora.operador("MR");
		calculadora.operador("/");
		calculadora.ingreso('3');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(2, resultado,0);
	}

	@Test
	public void calculosConLaMemoria2() {

		calculadora.ingreso('2');
		calculadora.operador("M+");
		calculadora.borrarTodo();
		calculadora.operador("MR");
		calculadora.operador("*");
		calculadora.operador("MR");
		calculadora.operador("*");
		calculadora.operador("MR");
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(8, resultado,0);
	}

	@Test
	public void calculosConLaMemoria3() {

		calculadora.ingreso('8');
		calculadora.operador("M+");
		calculadora.borrarTodo();
		calculadora.operador("MR");
		calculadora.operador("*");
		calculadora.operador("MR");
		calculadora.operador("/");
		calculadora.ingreso('4');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(16, resultado,0);
	}
	@Test
	public void calculosConLaMemoria4() {


		calculadora.ingreso('4');
		calculadora.operador("M-");
		calculadora.borrarTodo();
		calculadora.ingreso('3');
		calculadora.ingreso('2');
		calculadora.operador("/");
		calculadora.operador("MR");
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(-8, resultado,0);
	}

	@Test
	public void calculosConLaMemoria5() {

		calculadora.ingreso('1');
		calculadora.ingreso('2');
		calculadora.operador("/");
		calculadora.ingreso('1');
		calculadora.ingreso('3');
		calculadora.operador("=");;
		calculadora.operador("M+");
		calculadora.borrarTodo();
		calculadora.ingreso('5');
		calculadora.operador("*");
		calculadora.operador("MR");
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(4.6, resultado,0.1);
	}

	@Test
	public void calculosConLaMemoria6() {

		calculadora.ingreso('-');
		calculadora.ingreso('5');
		calculadora.operador("M+");
		calculadora.borrarTodo();
		calculadora.operador("MR");
		calculadora.operador("/");
		calculadora.ingreso('1');
		calculadora.ingreso('1');
		calculadora.operador("=");;
		calculadora.operador("*");
		calculadora.ingreso('-');
		calculadora.ingreso('2');
		calculadora.operador("=");;
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(0.9, resultado,0.1);
	}
	@Test
	public void calculosConLaMemoria7() {

		calculadora.ingreso('-');
		calculadora.ingreso('6');
		calculadora.operador("M+");
		calculadora.borrarTodo();
		calculadora.operador("MR");
		calculadora.operador("/");
		calculadora.ingreso('3');
		calculadora.operador("=");
		calculadora.operador("MC");
		calculadora.operador("MR");
		double resultado=Double.valueOf(calculadora.mostrar());

		assertEquals(-2, resultado,0);
	}
}
