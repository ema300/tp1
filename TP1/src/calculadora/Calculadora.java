package calculadora;

import java.awt.EventQueue;

import javax.swing.JFrame;

import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class Calculadora {

	private JFrame frame;
	private JTextField primeraPantalla;
	private JTextField segundaPantalla;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Calculadora window = new Calculadora();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Calculadora() {
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		CalculosBasicos calculos=new CalculosBasicos();
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.BLACK);
		frame.getContentPane().setForeground(Color.BLACK);
		frame.setBackground(Color.GRAY);
		frame.setBounds(100, 100, 431, 402);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JButton boton1 = new JButton("1");
		boton1.setBounds(10, 218, 89, 30);
		boton1.setBackground(Color.DARK_GRAY);
		boton1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		boton1.setForeground(Color.WHITE);
		boton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.ingreso('1');			
				segundaPantalla.setText(calculos.mostrarDos());
			}
		});

		JButton boton2 = new JButton("2");
		boton2.setBounds(109, 218, 89, 30);
		boton2.setBackground(Color.DARK_GRAY);
		boton2.setFont(new Font("Tahoma", Font.PLAIN, 13));
		boton2.setForeground(Color.WHITE);
		boton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.ingreso('2');			
				segundaPantalla.setText(calculos.mostrarDos());
			}
		});
		JButton boton3 = new JButton("3");
		boton3.setBounds(208, 218, 89, 30);
		boton3.setBackground(Color.DARK_GRAY);
		boton3.setFont(new Font("Tahoma", Font.PLAIN, 13));
		boton3.setForeground(Color.WHITE);
		boton3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.ingreso('3');			
				segundaPantalla.setText(calculos.mostrarDos());
			}
		});
		JButton boton4 = new JButton("4");
		boton4.setBounds(10, 252, 89, 30);
		boton4.setBackground(Color.DARK_GRAY);
		boton4.setFont(new Font("Tahoma", Font.PLAIN, 13));
		boton4.setForeground(Color.WHITE);
		boton4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.ingreso('4');			
				segundaPantalla.setText(calculos.mostrarDos());
			}
		});
		JButton boton5 = new JButton("5");
		boton5.setBounds(109, 252, 89, 30);
		boton5.setBackground(Color.DARK_GRAY);
		boton5.setFont(new Font("Tahoma", Font.PLAIN, 13));
		boton5.setForeground(Color.WHITE);
		boton5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.ingreso('5');			
				segundaPantalla.setText(calculos.mostrarDos());
			}
		});
		JButton boton6 = new JButton("6");
		boton6.setBounds(208, 252, 89, 30);
		boton6.setBackground(Color.DARK_GRAY);
		boton6.setFont(new Font("Tahoma", Font.PLAIN, 13));
		boton6.setForeground(Color.WHITE);
		boton6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.ingreso('6');			
				segundaPantalla.setText(calculos.mostrarDos());
			}
		});
		JButton boton7 = new JButton("7");
		boton7.setBounds(10, 287, 89, 30);
		boton7.setBackground(Color.DARK_GRAY);
		boton7.setFont(new Font("Tahoma", Font.PLAIN, 13));
		boton7.setForeground(Color.WHITE);
		boton7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.ingreso('7');			
				segundaPantalla.setText(calculos.mostrarDos());
			}
		});
		JButton boton8 = new JButton("8");
		boton8.setBounds(109, 287, 89, 30);
		boton8.setBackground(Color.DARK_GRAY);
		boton8.setFont(new Font("Tahoma", Font.PLAIN, 13));
		boton8.setForeground(Color.WHITE);
		boton8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.ingreso('8');			
				segundaPantalla.setText(calculos.mostrarDos());
			}
		});
		JButton boton9 = new JButton("9");
		boton9.setBounds(208, 287, 89, 30);
		boton9.setBackground(Color.DARK_GRAY);
		boton9.setFont(new Font("Tahoma", Font.PLAIN, 13));
		boton9.setForeground(Color.WHITE);
		boton9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.ingreso('9');			
				segundaPantalla.setText(calculos.mostrarDos());
			}
		});
		JButton boton0 = new JButton("0");
		boton0.setBounds(10, 322, 89, 30);
		boton0.setBackground(Color.DARK_GRAY);
		boton0.setFont(new Font("Tahoma", Font.PLAIN, 13));
		boton0.setForeground(Color.WHITE);
		boton0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.ingreso('0');			
				segundaPantalla.setText(calculos.mostrarDos());
			}
		});
		JButton botonPunto = new JButton(".");
		botonPunto.setBounds(109, 322, 89, 30);
		botonPunto.setBackground(Color.DARK_GRAY);
		botonPunto.setFont(new Font("Tahoma", Font.PLAIN, 13));
		botonPunto.setForeground(Color.WHITE);
		botonPunto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.ingreso('.');			
				segundaPantalla.setText(calculos.mostrarDos());
			}
		});
		JButton botonIgual = new JButton("=");
		botonIgual.setBounds(307, 322, 89, 30);
		botonIgual.setFont(new Font("Tahoma", Font.PLAIN, 16));
		botonIgual.setForeground(Color.WHITE);
		botonIgual.setBackground(new Color(30, 144, 255));
		botonIgual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.operador("=");	
				primeraPantalla.setText(calculos.mostrar());
				
			}
		});
		JButton botonMultiplicacion = new JButton("x");
		botonMultiplicacion.setBounds(307, 182, 89, 30);
		botonMultiplicacion.setFont(new Font("Tahoma", Font.PLAIN, 16));
		botonMultiplicacion.setForeground(Color.WHITE);
		botonMultiplicacion.setBackground(new Color(30, 144, 255));
		botonMultiplicacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.operador("*");	
				segundaPantalla.setText(calculos.mostrarDos());
			}
		});
		JButton botonDivision = new JButton("/");
		botonDivision.setBounds(307, 218, 89, 30);
		botonDivision.setFont(new Font("Tahoma", Font.PLAIN, 16));
		botonDivision.setForeground(Color.WHITE);
		botonDivision.setBackground(new Color(30, 144, 255));
		botonDivision.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.operador("/");			
				segundaPantalla.setText(calculos.mostrarDos());
			}
		});
		JButton botonResta = new JButton("-");
		botonResta.setBounds(307, 252, 89, 30);
		botonResta.setFont(new Font("Tahoma", Font.PLAIN, 16));
		botonResta.setForeground(Color.WHITE);
		botonResta.setBackground(new Color(30, 144, 255));
		botonResta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.operador("-");
				segundaPantalla.setText(calculos.mostrarDos());
			}
		});
		JButton botonSuma = new JButton("+");
		botonSuma.setBounds(307, 287, 89, 30);
		botonSuma.setFont(new Font("Tahoma", Font.PLAIN, 16));
		botonSuma.setForeground(Color.WHITE);
		botonSuma.setBackground(new Color(30, 144, 255));
		botonSuma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.operador("+");
				segundaPantalla.setText(calculos.mostrarDos());}
		});
		JButton botonBorrarDigito = new JButton("C");
		botonBorrarDigito.setBounds(307, 146, 89, 30);
		botonBorrarDigito.setFont(new Font("Tahoma", Font.PLAIN, 16));
		botonBorrarDigito.setForeground(Color.WHITE);
		botonBorrarDigito.setBackground(Color.RED);
		botonBorrarDigito.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.borrarUltimoDigito();	
				calculos.borrarUltimoDigitoPantallaDos();
				segundaPantalla.setText(calculos.mostrarDos());
			}
		});
		JButton botonBorrarTodo = new JButton("AC");
		botonBorrarTodo.setBounds(208, 146, 89, 30);
		botonBorrarTodo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		botonBorrarTodo.setForeground(Color.WHITE);
		botonBorrarTodo.setBackground(Color.RED);
		botonBorrarTodo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.borrarTodo();		
				calculos.borrarDigitosPantallaDos();
				primeraPantalla.setText(calculos.mostrar());
				segundaPantalla.setText(calculos.mostrarDos());
			}
		});
		
		JButton button = new JButton("(-)");
		button.setBounds(208, 322, 89, 30);
		button.setBackground(Color.DARK_GRAY);
		button.setFont(new Font("Tahoma", Font.PLAIN, 13));
		button.setForeground(Color.WHITE);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.ingreso('-');			
				segundaPantalla.setText(calculos.mostrarDos());
			}
		});
		
		JButton botonMMenos = new JButton("M-");
		botonMMenos.setBounds(208, 182, 89, 30);
		botonMMenos.setBackground(Color.YELLOW);
		botonMMenos.setForeground(Color.WHITE);
		botonMMenos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.operador("M-");			
			}	
		});
		
		JButton botonMMas = new JButton("M+");
		botonMMas.setBounds(109, 182, 89, 30);
		botonMMas.setBackground(Color.YELLOW);
		botonMMas.setForeground(Color.WHITE);
		botonMMas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.operador("M+");			
			}	
		});

		
		JButton botonMR = new JButton("MR");
		botonMR.setBounds(10, 182, 89, 30);
		botonMR.setBackground(Color.YELLOW);
		botonMR.setForeground(Color.WHITE);
		botonMR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.operador("MR");
				primeraPantalla.setText(calculos.mostrar());
				segundaPantalla.setText(calculos.mostrarDos());
			}	
		});
		JButton botonMC = new JButton("MC");
		botonMC.setForeground(Color.WHITE);
		botonMC.setBackground(Color.YELLOW);
		botonMC.setBounds(10, 146, 89, 30);
		botonMC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculos.operador("MC");
				
			}	
		});
		JLabel label = new JLabel("");
		label.setBounds(400, 151, 46, 14);
		frame.getContentPane().setLayout(null);

		primeraPantalla = new JTextField();
		primeraPantalla.setBounds(10, 54, 386, 67);
		primeraPantalla.setBackground(Color.LIGHT_GRAY);
		primeraPantalla.setSelectionColor(Color.LIGHT_GRAY);
		primeraPantalla.setFont(new Font("Britannic Bold", Font.PLAIN, 33));
		primeraPantalla.setEditable(false);
		primeraPantalla.setText("");
		primeraPantalla.setColumns(10);
		frame.getContentPane().add(primeraPantalla);
		frame.getContentPane().add(boton4);
		frame.getContentPane().add(boton5);
		frame.getContentPane().add(boton6);
		frame.getContentPane().add(botonResta);
		frame.getContentPane().add(botonMR);
		frame.getContentPane().add(boton1);
		frame.getContentPane().add(boton2);
		frame.getContentPane().add(boton3);
		frame.getContentPane().add(botonDivision);
		frame.getContentPane().add(botonMMas);
		frame.getContentPane().add(botonMMenos);
		frame.getContentPane().add(botonBorrarTodo);
		frame.getContentPane().add(botonMultiplicacion);
		frame.getContentPane().add(botonBorrarDigito);
		frame.getContentPane().add(boton7);
		frame.getContentPane().add(boton8);
		frame.getContentPane().add(boton9);
		frame.getContentPane().add(boton0);
		frame.getContentPane().add(botonPunto);
		frame.getContentPane().add(button);
		frame.getContentPane().add(botonIgual);
		frame.getContentPane().add(botonSuma);
		frame.getContentPane().add(label);	
		frame.getContentPane().add(botonMC);
		
		segundaPantalla = new JTextField();
		segundaPantalla.setEditable(false);
		segundaPantalla.setToolTipText("");
		segundaPantalla.setText("");
		segundaPantalla.setForeground(Color.BLACK);
		segundaPantalla.setSelectionColor(Color.LIGHT_GRAY);
		segundaPantalla.setFont(new Font("Britannic Bold", Font.PLAIN, 33));
		segundaPantalla.setColumns(10);
		segundaPantalla.setBackground(Color.LIGHT_GRAY);
		segundaPantalla.setBounds(10, 11, 386, 45);
		frame.getContentPane().add(segundaPantalla);
	}
}