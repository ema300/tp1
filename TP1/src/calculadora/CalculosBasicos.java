package calculadora;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CalculosBasicos {
	private StringBuilder ingresoEnPantalla;
	private StringBuilder ingresoEnPantallaDos;
	private String primertermino;
	private String segundotermino;
	private boolean resuelto;
	private String tercertermino;
	private String operador1;
	private String operador2;
	private boolean primerterminoOK;
	private boolean segundoterminoOK;
	private boolean esperartercertermino;
	private boolean ultimaTeclaEsOperando;

	// para operaciones de memoria
	public String memoria;
	public String auxiliaroperaciones;

	public CalculosBasicos() { ////// Constructores

		ingresoEnPantalla = new StringBuilder();
		ingresoEnPantallaDos = new StringBuilder();
		operador1 = "";
		operador2 = "";
		primertermino = "";
		segundotermino = "";
		tercertermino = "";

		resuelto = false;
		primerterminoOK = false;
		segundoterminoOK = false;
		esperartercertermino = false;
		ultimaTeclaEsOperando = false;

		memoria = "0";
		auxiliaroperaciones = "";

	}

	//////////////////// Captar lo escrito en pantalla
	private void primerTermino() {
		primertermino = ingresoEnPantalla.toString();

	}

	private void segundoTermino() {
		segundotermino = ingresoEnPantalla.toString();

	}

	private void tercerTermino() {
		tercertermino = ingresoEnPantalla.toString();

	}

	//////////////////// Ingreso a pantalla
	public void ingreso(char c) {
		if (this.resuelto == true) {
			vaciarTerminos();
			borrarDigitos();
		}
		this.resuelto = false;
		if (esperartercertermino == true) {
			borrarDigitos();
			esperartercertermino = false;
		}

		if (this.resuelto == false) {

			if (verificacionDeDatosValidosAntesDeIngresar(c) == true) {
				ingresoEnPantalla.append(c);
				ingresoEnPantallaDos.append(c);
				ultimaTeclaEsOperando = false;

			}
		}

	}

////////Verificaciones de validez/////////////////
	private boolean verificacionDeDatosValidosAntesDeIngresar(char c) {
		boolean ok = true;
		switch (c) {
		case '.':
			ok = unaSolaComa(c);
			break;
		case '-':
			ok = unSoloSignoNegativo(c);
			break;

		}

		return ok;
	}

	private boolean unaSolaComa(char c) {
		int cont = 0;

		for (int i = 0; i < ingresoEnPantalla.length(); i++) {

			if (ingresoEnPantalla.charAt(i) == c) {
				cont++;
			}
		}
		if (cont == 0) {
			return true;
		}
		return false;
	}

	private boolean unSoloSignoNegativo(char c) {
		int cont = 0;

		for (int i = 0; i < ingresoEnPantalla.length(); i++) {

			if (ingresoEnPantalla.charAt(i) == c) {
				cont++;
			}
		}
		if (cont == 0 && ingresoEnPantalla.length() < 1) {
			return true;
		}
		return false;
	}

/////////////Eliminacion de datos /////////////////
	protected void borrarUltimoDigito() {
		if (ingresoEnPantalla.length() != 0) {
			ingresoEnPantalla.deleteCharAt(ingresoEnPantalla.length() - 1);
		}
		if (ingresoEnPantalla.length() == 0) {
			borrarTodo();
			borrarDigitosPantallaDos();
		}

	}

	public void borrarDigitos() {
		if (ingresoEnPantalla.length() != 0) {
			ingresoEnPantalla.delete(0, ingresoEnPantalla.length());
		}
	}

	protected void borrarUltimoDigitoPantallaDos() {

		if (ingresoEnPantallaDos.length() != 0) {
			ingresoEnPantallaDos.deleteCharAt(ingresoEnPantallaDos.length() - 1);
		}

	}

	public void borrarDigitosPantallaDos() {

		if (ingresoEnPantallaDos.length() != 0) {
			ingresoEnPantallaDos.delete(0, ingresoEnPantallaDos.length());
		}

	}

	private void vaciarTerminos() {
		this.primertermino = "";
		this.segundotermino = "";
		this.tercertermino = "";
	}

	public void borrarTodo() { // agregado 31/8 CL
		vaciarTerminos();
		borrarDigitos();
		operador1 = "";
		operador2 = "";

		resuelto = false;
		primerterminoOK = false;
		segundoterminoOK = false;
		esperartercertermino = false;

	}

	////////////// eleccion de operacion///////////////
	public void operador(String operacion) {
		switch (operacion) {
		case "MR":
			recuperarMemoria();
			break;
		case "MC":
			borrarMemoria();
			break;
		case "M+":
			sumarMemoria();
			break;
		case "M-":
			restarMemoria();
			break;
		default:
			if (ultimaTeclaEsOperando == false) {
				if (operacion != "="&&ingresoEnPantalla.length()!=0)/// para que no me muestre en pantalla el "="
					ingresoEnPantallaDos.append(operacion);
				if (primerterminoOK == false&&operacion!="=")
					procesarPrimeraOperacion(operacion);
				else if (segundoterminoOK == false)
					procesarSegundaOperacion(operacion);
				else
					procesarTercerOperacion(operacion);
			}
			break;
		}
	}

	private void procesarPrimeraOperacion(String operacion) {
		if (this.ingresoEnPantalla.length() >= 1) {
			operador1 = operacion;
			primerTermino();// genera un primer termino
			borrarDigitos(); // borra los digitos ingresados en consola
			primerterminoOK = true; // Flag primer termino ingresado y procesado
			ultimaTeclaEsOperando = true;
		}

	}

	private void procesarSegundaOperacion(String operacion) {
		operador2 = operacion;
		segundoTermino();// genera un segundo termino
		borrarDigitos(); // borra los digitos ingresados en consola
		segundoterminoOK = true; // Flag segundo termino ingresado y procesado

		switch (operador2) {
		case "=":
			finCalculo();
			borrarDigitosPantallaDos();

			break;

		case "+":
			CalculoDosTerminos();
			ultimaTeclaEsOperando = true;
			break;
		case "-":
			CalculoDosTerminos();
			ultimaTeclaEsOperando = true;
			break;
		case "*":
			if (operador1 == "*" || operador1 == "/")
				CalculoDosTerminos();
			esperartercertermino = true;
			ultimaTeclaEsOperando = true;
			break;
		case "/":
			if (operador1 == "*" || operador1 == "/")
				CalculoDosTerminos();
			esperartercertermino = true;
			ultimaTeclaEsOperando = true;
			break;
		}

	}

	private void procesarTercerOperacion(String operacion) {

		tercerTermino();
		borrarDigitos();

		switch (operacion) {
		case "=":
			finCalculoTresTerminos();
			borrarDigitosPantallaDos();
			break;

		case "+":
			sumarRestarTresTerminos();
			operador1 = "+";
			ultimaTeclaEsOperando = true;
			break;
		case "-":
			sumarRestarTresTerminos();
			operador1 = "-";
			ultimaTeclaEsOperando = true;
			break;
		case "*":
			if (operador2 == "*")
				segundotermino = multiplicarTresTerminos();
			if (operador2 == "/")
				segundotermino = dividirTresTerminos();
			operador2 = "*";
			ultimaTeclaEsOperando = true;
			break;
		case "/":
			if (operador2 == "*")
				segundotermino = multiplicarTresTerminos();
			if (operador2 == "/")
				segundotermino = dividirTresTerminos();
			operador2 = "/";
			ultimaTeclaEsOperando = true;
			break;
		}

	}

	private void finCalculo() {
		finalizacionDelCalculo();
		vaciarTerminos();
		operador1 = "";
		operador2 = "";
		primerterminoOK = false;
		segundoterminoOK = false;
		resuelto = false;
	}

	private void finCalculoTresTerminos() {
		if (operador2 == "*")
			segundotermino = multiplicarTresTerminos();
		if (operador2 == "/")
			segundotermino = dividirTresTerminos();
		if (operador1 == "+")
			ingresoEnPantalla.append(sumar());
		if (operador1 == "-")
			ingresoEnPantalla.append(restar());
	
		vaciarTerminos();
		operador1 = "";
		operador2 = "";
		primerterminoOK = false;
		segundoterminoOK = false;
		resuelto = false;

	}

	private void finalizacionDelCalculo() {
		switch (operador1) {
		case "+":
			ingresoEnPantalla.append(sumar());
			break;
		case "-":
			ingresoEnPantalla.append(restar());
			break;
		case "*":
			ingresoEnPantalla.append(multiplicar());
			break;
		case "/":
			ingresoEnPantalla.append(dividir());
			break;
		}
	}

	private void CalculoDosTerminos() {
		switch (operador1) {
		case "+":
			ingresoEnPantalla.append(sumar());
			vaciarTerminos();
			primertermino = ingresoEnPantalla.toString();
			operador1 = operador2;
			operador2 = "";
			esperartercertermino = true;
			segundoterminoOK = false;
			break;
		case "-":
			ingresoEnPantalla.append(restar());
			vaciarTerminos();
			primertermino = ingresoEnPantalla.toString();
			operador1 = operador2;
			operador2 = "";
			esperartercertermino = true;
			segundoterminoOK = false;
			break;
		case "*":
			ingresoEnPantalla.append(multiplicar());
			vaciarTerminos();
			primertermino = ingresoEnPantalla.toString();
			operador1 = operador2;
			operador2 = "";
			esperartercertermino = true;
			segundoterminoOK = false;
			break;
		case "/":
			ingresoEnPantalla.append(dividir());
			vaciarTerminos();
			primertermino = ingresoEnPantalla.toString();
			operador1 = operador2;
			operador2 = "";
			esperartercertermino = true;
			segundoterminoOK = false;
			break;

		}

	}

	//////////////// OPERACIONES/////////////////
	////// OperacionesSimples//////
	private String sumar() {

		BigDecimal a = new BigDecimal(this.primertermino);
		BigDecimal b = new BigDecimal(this.segundotermino);
		BigDecimal resultado = new BigDecimal("0.0");
		resultado = a.add(b);
		return String.valueOf(resultado);

	}

	private String restar() {
		BigDecimal a = new BigDecimal(this.primertermino);
		BigDecimal b = new BigDecimal(this.segundotermino);
		BigDecimal resultado = new BigDecimal("0.0");
		resultado = a.subtract(b);
		return String.valueOf(resultado);

	}

	private String dividir() {
		String resultadoDivision;
		resultadoDivision = "";
		BigDecimal a = new BigDecimal(this.primertermino);
		BigDecimal b = new BigDecimal(this.segundotermino);
		if (b.compareTo(BigDecimal.ZERO) == 0 || this.segundotermino == "0") {
			borrarTodo();
		} else {
			BigDecimal resultado = new BigDecimal("0.0");
			resultado = a.divide(b, 6, RoundingMode.CEILING);
			resultadoDivision = String.valueOf(resultado.stripTrailingZeros().toPlainString());
		}

		return resultadoDivision;

	}

	private String multiplicar() {

		BigDecimal a = new BigDecimal(this.primertermino);
		BigDecimal b = new BigDecimal(this.segundotermino);
		BigDecimal resultado = new BigDecimal("0.0");
		resultado = a.multiply(b);
		return String.valueOf(resultado);
	}

	///////////// Operaciones para casos con tres terminos /////////////
	private void sumarRestarTresTerminos() {

		if (operador2 == "*")
			segundotermino = multiplicarTresTerminos();
		if (operador2 == "/")
			segundotermino = dividirTresTerminos();
		if (operador1 == "+")
			ingresoEnPantalla.append(sumar());
		if (operador1 == "-")
			ingresoEnPantalla.append(restar());
		vaciarTerminos();
		primerTermino();
		operador2 = "";
		segundoterminoOK = false;
		esperartercertermino = true;

	}

	private String multiplicarTresTerminos() {
		BigDecimal a = new BigDecimal(this.segundotermino);
		BigDecimal b = new BigDecimal(this.tercertermino);
		BigDecimal resultado = new BigDecimal("0.0");
		resultado = a.multiply(b);
		return String.valueOf(resultado);

	}

	private String dividirTresTerminos() {
		String resultadoDivision;
		resultadoDivision = "";
		BigDecimal a = new BigDecimal(this.segundotermino);
		BigDecimal b = new BigDecimal(this.tercertermino);
		if (b.compareTo(BigDecimal.ZERO) == 0 || this.tercertermino == "0") {
			borrarTodo();
		} else {
			BigDecimal resultado = new BigDecimal("0.0");
			resultado = a.divide(b, 6, RoundingMode.CEILING);
			resultadoDivision = String.valueOf(resultado.stripTrailingZeros().toPlainString());
		}
		return resultadoDivision;
	}

	///////// OPERACIONES DE MEMORIA ////////
	public void recuperarMemoria() {
		if (memoria != "0") {
			borrarDigitos();
			ingresoEnPantalla.append(memoria);
			ultimaTeclaEsOperando = false;
		}
	}

	public void borrarMemoria() {
		memoria = "0";

	}

	public void sumarMemoria() {
		auxiliaroperaciones = ingresoEnPantalla.toString();
		if (ingresoEnPantalla.length() != 0) {
			auxiliaroperaciones = ingresoEnPantalla.toString();
			BigDecimal a = new BigDecimal(memoria);
			BigDecimal b = new BigDecimal(auxiliaroperaciones);
			BigDecimal resultado = new BigDecimal("0.0");
			resultado = a.add(b);
			if (resultado.compareTo(BigDecimal.ZERO) == 0)
				memoria = "0";
			else {
				memoria = String.valueOf(resultado);
			}
			auxiliaroperaciones = "0";
		}
	}

	public void restarMemoria() {
		auxiliaroperaciones = ingresoEnPantalla.toString();
		if (ingresoEnPantalla.length() != 0) {
			auxiliaroperaciones = ingresoEnPantalla.toString();
			BigDecimal a = new BigDecimal(memoria);
			BigDecimal b = new BigDecimal(auxiliaroperaciones);
			BigDecimal resultado = new BigDecimal("0.0");
			resultado = a.subtract(b);
			if (resultado.compareTo(BigDecimal.ZERO) == 0)
				memoria = "0";
			else {
				memoria = String.valueOf(resultado);
			}
			auxiliaroperaciones = "0";
		}
	}

	//////////////// imprime en pantalla///////////////
	public String mostrar() {
		return ingresoEnPantalla.toString();

	}

	public String mostrarDos() {
		return ingresoEnPantallaDos.toString();

	}
}
